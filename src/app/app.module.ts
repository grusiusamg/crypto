import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { CoinSelectComponent } from './coin-select/coin-select.component';
import { ZrxComponent } from './coins/zrx/zrx.component';
import { RouterModule } from '@angular/router';
import {BtcComponent} from './coins/btc/btc.component';
import { AsafeComponent } from './coins/asafe/asafe.component';
import { AbetComponent } from './coins/abet/abet.component';
import { BatComponent } from './coins/bat/bat.component';
import { BnbComponent } from './coins/bnb/bnb.component';
import { BchComponent } from './coins/bch/bch.component';
import { BsvComponent } from './coins/bsv/bsv.component';
import { BttComponent } from './coins/btt/btt.component';
import { BlockComponent } from './coins/block/block.component';
import { AdaComponent } from './coins/ada/ada.component';
import { LinkComponent } from './coins/link/link.component';
import { CtscComponent } from './coins/ctsc/ctsc.component';
import { DashComponent } from './coins/dash/dash.component';
import { DgbComponent } from './coins/dgb/dgb.component';
import { DogeComponent } from './coins/doge/doge.component';
import { EtnComponent } from './coins/etn/etn.component';
import { EosComponent } from './coins/eos/eos.component';
import { EsbcComponent } from './coins/esbc/esbc.component';
import { EthComponent } from './coins/eth/eth.component';
import { EtcComponent } from './coins/etc/etc.component';
import { XpComponent } from './coins/xp/xp.component';
import { GrcComponent } from './coins/grc/grc.component';
import { ZenComponent } from './coins/zen/zen.component';
import { KybcComponent } from './coins/kybc/kybc.component';
import { KmdComponent } from './coins/kmd/kmd.component';
import { LtcComponent } from './coins/ltc/ltc.component';
import { MmoComponent } from './coins/mmo/mmo.component';
import { XmrComponent } from './coins/xmr/xmr.component';
import { NanoComponent } from './coins/nano/nano.component';
import { XemComponent } from './coins/xem/xem.component';
import { NeoComponent } from './coins/neo/neo.component';
import { PirateComponent } from './coins/pirate/pirate.component';
import { PivxComponent } from './coins/pivx/pivx.component';
import { PrivComponent } from './coins/priv/priv.component';
import { NpsxComponent } from './coins/npsx/npsx.component';
import { RvnComponent } from './coins/rvn/rvn.component';
import { RddComponent } from './coins/rdd/rdd.component';
import { SccComponent } from './coins/scc/scc.component';
import { XlmComponent } from './coins/xlm/xlm.component';
import { SysComponent } from './coins/sys/sys.component';
import { UsdtComponent } from './coins/usdt/usdt.component';
import { XtzComponent } from './coins/xtz/xtz.component';
import { TrxComponent } from './coins/trx/trx.component';
import { TrboComponent } from './coins/trbo/trbo.component';
import { UsdcComponent } from './coins/usdc/usdc.component';
import { XvgComponent } from './coins/xvg/xvg.component';
import { VsxComponent } from './coins/vsx/vsx.component';
import { XrpComponent } from './coins/xrp/xrp.component';
import { ZecComponent } from './coins/zec/zec.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    CoinSelectComponent,
    ZrxComponent,
    AsafeComponent,
    AbetComponent,
    BatComponent,
    BnbComponent,
    BchComponent,
    BsvComponent,
    BttComponent,
    BlockComponent,
    AdaComponent,
    LinkComponent,
    CtscComponent,
    DashComponent,
    DgbComponent,
    DogeComponent,
    EtnComponent,
    EosComponent,
    EsbcComponent,
    EthComponent,
    EtcComponent,
    XpComponent,
    GrcComponent,
    ZenComponent,
    KybcComponent,
    KmdComponent,
    LtcComponent,
    MmoComponent,
    XmrComponent,
    NanoComponent,
    XemComponent,
    NeoComponent,
    PirateComponent,
    PivxComponent,
    PrivComponent,
    NpsxComponent,
    RvnComponent,
    RddComponent,
    SccComponent,
    XlmComponent,
    SysComponent,
    UsdtComponent,
    XtzComponent,
    TrxComponent,
    TrboComponent,
    UsdcComponent,
    XvgComponent,
    VsxComponent,
    XrpComponent,
    ZecComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {path: 'btc', component: BtcComponent},
      {path: 'zrx', component: ZrxComponent},
      {path: 'asafe', component: AsafeComponent},
      {path: 'abet', component: AbetComponent},
      {path: 'bat', component: BatComponent},
      {path: 'bnb', component: BnbComponent},
      {path: 'bsv', component: BsvComponent},
      {path: 'btt', component: BttComponent},
      {path: 'block', component: BlockComponent},
      {path: 'ada', component: AdaComponent},
      {path: 'link', component: LinkComponent},
      {path: 'bch', component: BchComponent},
      {path: 'ctsc', component: CtscComponent},
      {path: 'dash', component: DashComponent},
      {path: 'dgb', component: DgbComponent},
      {path: 'doge', component: DogeComponent},
      {path: 'etn', component: EtnComponent},
      {path: 'eos', component: EosComponent},
      {path: 'esbc', component: EsbcComponent},
      {path: 'eth', component: EthComponent},
      {path: 'etc', component: EtcComponent},
      {path: 'xp', component: XpComponent},
      {path: 'grc', component: GrcComponent},
      {path: 'zen', component: ZenComponent},
      {path: 'kydc', component: KybcComponent},
      {path: 'kmd', component: KmdComponent},
      {path: 'ltc', component: LtcComponent},
      {path: 'mmo', component: MmoComponent},
      {path: 'xmr', component: XmrComponent},
      {path: 'nano', component: NanoComponent},
      {path: 'xem', component: XemComponent},
      {path: 'neo', component: NeoComponent},
      {path: 'pirate', component: PirateComponent},
      {path: 'pivx', component: PivxComponent},
      {path: 'priv', component: PrivComponent},
      {path: 'npsx', component: NpsxComponent},
      {path: 'rvn', component: RvnComponent},
      {path: 'rdd', component: RddComponent},
      {path: 'scc', component: SccComponent},
      {path: 'xlm', component: XlmComponent},
      {path: 'sys', component: SysComponent},
      {path: 'usdt', component: UsdtComponent},
      {path: 'xtz', component: XtzComponent},
      {path: 'trx', component: TrxComponent},
      {path: 'trbo', component: TrboComponent},
      {path: 'usdc', component: UsdcComponent},
      {path: 'xvg', component: XvgComponent},
      {path: 'vsx', component: VsxComponent},
      {path: 'xrp', component: XrpComponent},
      {path: 'zec', component: ZecComponent},
    ]),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
