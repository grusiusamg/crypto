import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrboComponent } from './trbo.component';

describe('TrboComponent', () => {
  let component: TrboComponent;
  let fixture: ComponentFixture<TrboComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrboComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrboComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
