import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZecComponent } from './zec.component';

describe('ZecComponent', () => {
  let component: ZecComponent;
  let fixture: ComponentFixture<ZecComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ZecComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ZecComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
