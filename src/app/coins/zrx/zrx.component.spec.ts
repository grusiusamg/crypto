import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZrxComponent } from './zrx.component';

describe('ZrxComponent', () => {
  let component: ZrxComponent;
  let fixture: ComponentFixture<ZrxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ZrxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ZrxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
