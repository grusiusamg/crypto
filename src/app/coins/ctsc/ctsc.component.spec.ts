import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CtscComponent } from './ctsc.component';

describe('CtscComponent', () => {
  let component: CtscComponent;
  let fixture: ComponentFixture<CtscComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CtscComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CtscComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
