import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EsbcComponent } from './esbc.component';

describe('EsbcComponent', () => {
  let component: EsbcComponent;
  let fixture: ComponentFixture<EsbcComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EsbcComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EsbcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
