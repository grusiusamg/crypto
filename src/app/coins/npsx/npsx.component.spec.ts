import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NpsxComponent } from './npsx.component';

describe('NpsxComponent', () => {
  let component: NpsxComponent;
  let fixture: ComponentFixture<NpsxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NpsxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NpsxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
