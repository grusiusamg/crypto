import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EtnComponent } from './etn.component';

describe('EtnComponent', () => {
  let component: EtnComponent;
  let fixture: ComponentFixture<EtnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EtnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
