import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RvnComponent } from './rvn.component';

describe('RvnComponent', () => {
  let component: RvnComponent;
  let fixture: ComponentFixture<RvnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RvnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RvnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
