import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DogeComponent } from './doge.component';

describe('DogeComponent', () => {
  let component: DogeComponent;
  let fixture: ComponentFixture<DogeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DogeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DogeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
