import { ComponentFixture, TestBed } from '@angular/core/testing';

import { XtzComponent } from './xtz.component';

describe('XtzComponent', () => {
  let component: XtzComponent;
  let fixture: ComponentFixture<XtzComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ XtzComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(XtzComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
