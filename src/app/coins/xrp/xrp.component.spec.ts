import { ComponentFixture, TestBed } from '@angular/core/testing';

import { XrpComponent } from './xrp.component';

describe('XrpComponent', () => {
  let component: XrpComponent;
  let fixture: ComponentFixture<XrpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ XrpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(XrpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
