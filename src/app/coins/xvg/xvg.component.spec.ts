import { ComponentFixture, TestBed } from '@angular/core/testing';

import { XvgComponent } from './xvg.component';

describe('XvgComponent', () => {
  let component: XvgComponent;
  let fixture: ComponentFixture<XvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ XvgComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(XvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
