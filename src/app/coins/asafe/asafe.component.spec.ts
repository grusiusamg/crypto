import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AsafeComponent } from './asafe.component';

describe('AsafeComponent', () => {
  let component: AsafeComponent;
  let fixture: ComponentFixture<AsafeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AsafeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AsafeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
