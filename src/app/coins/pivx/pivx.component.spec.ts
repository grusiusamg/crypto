import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PivxComponent } from './pivx.component';

describe('PivxComponent', () => {
  let component: PivxComponent;
  let fixture: ComponentFixture<PivxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PivxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PivxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
