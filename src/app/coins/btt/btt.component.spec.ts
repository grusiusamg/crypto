import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BttComponent } from './btt.component';

describe('BttComponent', () => {
  let component: BttComponent;
  let fixture: ComponentFixture<BttComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BttComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BttComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
