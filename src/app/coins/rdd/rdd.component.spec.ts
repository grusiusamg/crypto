import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RddComponent } from './rdd.component';

describe('RddComponent', () => {
  let component: RddComponent;
  let fixture: ComponentFixture<RddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
