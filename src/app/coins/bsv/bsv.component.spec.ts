import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BsvComponent } from './bsv.component';

describe('BsvComponent', () => {
  let component: BsvComponent;
  let fixture: ComponentFixture<BsvComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BsvComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BsvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
