import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VsxComponent } from './vsx.component';

describe('VsxComponent', () => {
  let component: VsxComponent;
  let fixture: ComponentFixture<VsxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VsxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VsxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
