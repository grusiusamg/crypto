import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EosComponent } from './eos.component';

describe('EosComponent', () => {
  let component: EosComponent;
  let fixture: ComponentFixture<EosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
