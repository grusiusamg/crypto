import { ComponentFixture, TestBed } from '@angular/core/testing';

import { XlmComponent } from './xlm.component';

describe('XlmComponent', () => {
  let component: XlmComponent;
  let fixture: ComponentFixture<XlmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ XlmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(XlmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
