import { ComponentFixture, TestBed } from '@angular/core/testing';

import { XmrComponent } from './xmr.component';

describe('XmrComponent', () => {
  let component: XmrComponent;
  let fixture: ComponentFixture<XmrComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ XmrComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(XmrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
