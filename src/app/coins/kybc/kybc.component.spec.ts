import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KybcComponent } from './kybc.component';

describe('KybcComponent', () => {
  let component: KybcComponent;
  let fixture: ComponentFixture<KybcComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KybcComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KybcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
