import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UsdcComponent } from './usdc.component';

describe('UsdcComponent', () => {
  let component: UsdcComponent;
  let fixture: ComponentFixture<UsdcComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UsdcComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UsdcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
